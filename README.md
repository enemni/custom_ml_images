# Building custom images

Repository for building custom images to use for Kubeflow Notebook Servers, Pipelines and Katib.

## Autobuild
- Edit **FROM** tag in Dockerfile to set the base image
- Edit **Dockerfile** and **requirements.txt** for custom packages
- Check GitLab left panel - CI/CD - Pipelines
    - **With every push to this repo, a pipeline is created, docker image is being built**
- After pipeline completion, check Packages & Registries - Container registry
    - Copy the tag of the docker image

## Local
- `git clone https://gitlab.cern.ch/ai-ml/custom_ml_images.git`
- `cd custom_ml_images`
- Edit _Dockerfile_ and/or _requirements.txt_
- `docker build . -f Dockerfile -t gitlab-registry.cern.ch/ai-ml/CUSTOM_REPO_PATH`
- `docker push gitlab-registry.cern.ch/ai-ml/CUSTOM_REPO_PATH`
