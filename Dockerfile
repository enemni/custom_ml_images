# Select a base image from which to extend
FROM registry.cern.ch/ml/kf-14-pytorch-jupyter:v4
# or: FROM custom_public_registry/username/image

USER root

# Install required packages
RUN apt-get update &&\
    apt-get install -y binutils libproj-dev gdal-bin

COPY requirements.txt /requirements.txt
RUN apt-get -qq update && pip3 install -r /requirements.txt
RUN pip install torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cu92

USER jovyan

# The following line is mandatory:
CMD ["sh", "-c", \
     "jupyter lab --notebook-dir=/home/jovyan --ip=0.0.0.0 --no-browser \
      --allow-root --port=8888 --LabApp.token='' --LabApp.password='' \
      --LabApp.allow_origin='*' --LabApp.base_url=${NB_PREFIX}"]
